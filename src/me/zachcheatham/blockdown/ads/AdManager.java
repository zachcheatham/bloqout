package me.zachcheatham.blockdown.ads;

import me.zachcheatham.blockdown.util.ScreenSize;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class AdManager
{
	private FrameLayout window;
	private LinearLayout layout;
	private AdView ad;
	
	private boolean loaded = false;
	
	public void setup(Activity app)
	{
		window = (FrameLayout) app.getWindow().getDecorView().findViewById(android.R.id.content);
		
		layout = new LinearLayout(app);
		ad = new AdView(app);
		
		ad.setAdSize(AdSize.SMART_BANNER);
		ad.setAdUnitId("ca-app-pub-3614943156506959/1521672422");
		ad.setY(ScreenSize.getStatusBarSize());
		
		layout.addView(ad);
	}
	
	public void showGameoverAd()
	{	
		if (!loaded)
		{
			AdRequest.Builder arb = new AdRequest.Builder();
			arb.addTestDevice("CE0B68E879C7467E4EB191D4149FC973");
		
			ad.loadAd(arb.build());
			
			loaded = true;
		}
		else
		{
			ad.resume();
		}
		
		window.addView(layout);
	}
	
	public void hideGameoverAd()
	{
		ad.pause();
		window.removeView(layout);
	}
}
