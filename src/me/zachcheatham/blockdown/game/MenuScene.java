package me.zachcheatham.blockdown.game;

import me.zachcheatham.blockdown.resources.Textures;
import me.zachcheatham.blockdown.util.ButtonFactory;
import me.zachcheatham.blockdown.util.ScreenSize;
import me.zachcheatham.blockdown.util.Tag;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class MenuScene extends Scene
{
	public MenuScene(VertexBufferObjectManager vbom, OnClickListener clickListener)
	{
		setBackgroundEnabled(true);
		setBackground(new Background(0.0f, 0.0f, 0.0f, 1f));
		setTouchAreaBindingOnActionDownEnabled(true);
		
		float x = (ScreenSize.getScreenWidth() / 2) - (Textures.paused.getWidth() / 2);
		float y = (ScreenSize.getScreenHeight() / 4) - (Textures.paused.getHeight() / 2);
		
		Sprite paused = new Sprite(x, y, Textures.paused, vbom);
		attachChild(paused);
		
		ButtonSprite restartButton = ButtonFactory.createFullButton("Restart", Tag.BUTTON_RESTART, vbom, clickListener);
		ButtonSprite resumeButton = ButtonFactory.createFullButton("Resume", Tag.BUTTON_RESUME, vbom, clickListener);
		
		y = (ScreenSize.getScreenHeight() / 2) - ((restartButton.getHeight() * 2) / 2);
		
		restartButton.setY(y);
		resumeButton.setY(y + restartButton.getHeight());
		
		attachChild(restartButton);
		attachChild(resumeButton);
		
		registerTouchArea(restartButton);
		registerTouchArea(resumeButton);
	}
}
