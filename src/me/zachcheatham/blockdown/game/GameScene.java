package me.zachcheatham.blockdown.game;

import android.content.Context;
import me.zachcheatham.blockdown.Blockdown;
import me.zachcheatham.blockdown.R;
import me.zachcheatham.blockdown.game.block.BlockBlocker;
import me.zachcheatham.blockdown.game.block.BlockBomb;
import me.zachcheatham.blockdown.game.block.BlockPaint;
import me.zachcheatham.blockdown.game.block.BlockWildcard;
import me.zachcheatham.blockdown.resources.Fonts;
import me.zachcheatham.blockdown.resources.Sounds;
import me.zachcheatham.blockdown.resources.Textures;
import me.zachcheatham.blockdown.scene.BoardScene;
import me.zachcheatham.blockdown.util.ScreenSize;
import me.zachcheatham.blockdown.util.Tag;
import me.zachcheatham.blockdown.util.game.Achievement;
import me.zachcheatham.blockdown.util.game.Leaderboard;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.HorizontalAlign;

import com.google.android.gms.games.Games;

import android.content.SharedPreferences;

public class GameScene extends BoardScene implements OnClickListener
{
	private SharedPreferences sharedPreferences;

	private float scoreSpace;
	private float scoreMiddle;

	private Text scoreLabel;
	private Text tutorialText;
    private ButtonSprite pauseButton;
	private int points = 0;

	private boolean tutorialPickup;
	private boolean tutorialMatch;
	private boolean tutorialBlock;
	private Class tutorialBlockType;

	private boolean hasTutorialBoard;
	private boolean hasTutorialBlocker;
	private boolean hasTutorialBomb;
	private boolean hasTutorialPaint;
	private boolean hasTutorialWildcard;

	public GameScene(Blockdown activity)
	{
		super(activity);
	}

	@Override
	protected void createScene()
	{
		sharedPreferences = activity.getSharedPreferences(activity.getString(R.string.shared_preferences), Context.MODE_PRIVATE);

		hasTutorialBoard = sharedPreferences.getBoolean(activity.getString(R.string.shdprf_tutorial_board), false);
		hasTutorialBlocker = sharedPreferences.getBoolean(activity.getString(R.string.shdprf_tutorial_blocker), false);
		hasTutorialBomb = sharedPreferences.getBoolean(activity.getString(R.string.shdprf_tutorial_bomb), false);
		hasTutorialPaint = sharedPreferences.getBoolean(activity.getString(R.string.shdprf_tutorial_paint), false);
		hasTutorialWildcard = sharedPreferences.getBoolean(activity.getString(R.string.shdprf_tutorial_wildcard), false);

        scoreSpace = ScreenSize.getScreenHeight() - (grabber.getBlockSize() * 5 + BlockGrabber.BLOCK_MARGIN * 5) - ScreenSize.getActionBarSize() - ScreenSize.getStatusBarSize();
        scoreMiddle = ScreenSize.getStatusBarSize() + (grabber.getBlockSize() * 4 + BlockGrabber.BLOCK_MARGIN * 4) + (scoreSpace / 2);
        
        scoreLabel = new Text(0, scoreMiddle - (Fonts.scoreFont.getLineHeight() / 2.375f), Fonts.scoreFont, "0", 3, new TextOptions(HorizontalAlign.RIGHT), activity.getVertexBufferObjectManager());
        scoreLabel.setX((ScreenSize.getScreenWidth() / 2) - (scoreLabel.getWidth() / 2));
        attachChild(scoreLabel);
        
        pauseButton = new BoardButton(0, 0, Textures.buttonPause, activity.getVertexBufferObjectManager(), this, this);
        pauseButton.setTag(Tag.BUTTON_PAUSE);
        ScreenSize.scaleSprite(pauseButton);
        pauseButton.setX((grabber.getBlockSize() * 3 + BlockGrabber.BLOCK_MARGIN * 4) + (grabber.getBlockSize() / 2) - (pauseButton.getWidth() / 2));
        pauseButton.setY(scoreMiddle - (pauseButton.getHeight() / 2));
        attachChild(pauseButton);
        registerTouchArea(pauseButton);

		tutorialText = new Text(0, 0, Fonts.tutorialFont, "", 256, new TextOptions(HorizontalAlign.CENTER), activity.getVertexBufferObjectManager());
		tutorialText.setAutoWrap(AutoWrap.WORDS);
		tutorialText.setAutoWrapWidth(ScreenSize.getScreenWidth());
		tutorialText.setVisible(false);
		attachChild(tutorialText);

		if (!hasTutorialBoard)
			initTutorialPickup();

		grabber.refill(!hasTutorialBoard);
	}

	@Override
	public void onAttached()
	{
		super.onAttached();
	}
	
	protected void onBoardUpdate()
	{
		int points = board.checkMatches();
		
		if (points > 0)
		{
			Sounds.playMatch();

			if (tutorialMatch)
				completeTutorialMatching();

			setScore(this.points + points);
			
			if (activity.getGoogleGameHelper().isSignedIn())
				Games.Achievements.unlock(activity.getGoogleGameHelper().getApiClient(), Achievement.FIRST_MATCH);
			
			if (activity.getGoogleGameHelper().isSignedIn() && points > 1)
				Games.Achievements.unlock(activity.getGoogleGameHelper().getApiClient(), Achievement.MULTI_MATCH);
			
			if (activity.getGoogleGameHelper().isSignedIn() && points > 2)
				Games.Achievements.unlock(activity.getGoogleGameHelper().getApiClient(), Achievement.TRIPLE_MATCH);
			
			if (activity.getGoogleGameHelper().isSignedIn() && this.points >= 20)
				Games.Achievements.unlock(activity.getGoogleGameHelper().getApiClient(), Achievement.TWENTY_POINTS);
			
			if (activity.getGoogleGameHelper().isSignedIn() && this.points >= 200)
				Games.Achievements.unlock(activity.getGoogleGameHelper().getApiClient(), Achievement.MASTER);
		}
		
		if (board.isFull() && !grabber.containsGameSaver() && points == 0)
		{
			int highScore;
			if (activity.getGoogleGameHelper().isSignedIn())
                highScore = Leaderboard.getMainHighScore();
			else
                highScore = activity.getSharedPreferences(activity.getString(R.string.shared_preferences), Context.MODE_PRIVATE).getInt(activity.getString(R.string.shdprf_highscore), 0);
			
			setChildScene(new GameOverScene(this.points, highScore, activity.getGoogleGameHelper(), this, activity.getVertexBufferObjectManager(), this), false, true, true);
		}
		else if (grabber.isEmpty())
		{
			grabber.refill(tutorialMatch);

			if (!tutorialPickup && !tutorialMatch)
			{
				for (int i = 0; i < 4; i++)
				{
					BlockContainer container = grabber.getContainer(i);
					if (evalTutorialBlock(container))
					{
						for (int a = 0; a < 4; a++)
						{
							if (i != a)
								grabber.getContainer(a).setFaded(true);
						}

						break;
					}
				}
			}
		}
	}
	
	private void resetGame()
	{
		board.clear();
		grabber.refill(false);

		if (tutorialBlock)
		{
			tutorialBlockType = null; // Cause the completion function to not save anything.
			completeTutorialBlockFade();
			completeTutorialBlock();
		}
				
		setScore(0);
	}

	private void setScore(int score)
	{
		scoreLabel.setText("" + score);
		scoreLabel.setX((ScreenSize.getScreenWidth() / 2) - (scoreLabel.getWidth() / 2));
		
		points = score;
	}
	
	public boolean isMovingBlock()
	{
		return movingContainer.isVisible();
	}
	
	public void submitScore()
	{
		if (activity.getGoogleGameHelper().isSignedIn())
			Leaderboard.submitMainScore(activity.getGoogleGameHelper().getApiClient(), points);
		else
		{
			SharedPreferences s = activity.getSharedPreferences(activity.getString(R.string.shared_preferences), Context.MODE_PRIVATE);
			int highScore = s.getInt(activity.getString(R.string.shdprf_highscore), 0);
			
			if (highScore < points)
			{
				SharedPreferences.Editor e = s.edit();
				e.putInt(activity.getString(R.string.shdprf_highscore), points);
				e.apply();
			}
		}
	}

    @Override
    public void onContainerTouched(BlockContainer container, TouchEvent pSceneTouchEvent)
	{
		if (pSceneTouchEvent.isActionDown())
		{
			if (tutorialPickup)
				initTutorialMatch();
			else if (tutorialBlock && !container.isEmpty() && container.getBlock().getClass() == tutorialBlockType)
				completeTutorialBlockFade();
		}
		else if (pSceneTouchEvent.isActionUp())
		{
			if (tutorialBlock
					&& container.blockLocation == BlockLocation.BlockBoard
					&& (
						(!lastSelectedSpace.isEmpty() && lastSelectedSpace.getBlock().getClass() == tutorialBlockType)
						|| (!movingContainer.isEmpty() && movingContainer.getBlock().getClass() == tutorialBlockType))
					)
			{
				completeTutorialBlock();
			}
		}

        super.onContainerTouched(container, pSceneTouchEvent);
    }

	@Override
	public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY)
	{
		if (pButtonSprite.getTag() == Tag.BUTTON_RESTART)
		{
			clearChildScene();
			resetGame();
		}
		else if (pButtonSprite.getTag() == Tag.BUTTON_LEADERBOARD)
		{
			activity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(activity.getGoogleGameHelper().getApiClient(), Leaderboard.MAIN), 0);
		}
		else if (pButtonSprite.getTag() == Tag.BUTTON_RESUME)
		{
			clearChildScene();
		}
		else if (pButtonSprite.getTag() == Tag.BUTTON_PAUSE)
		{
			setChildScene(new MenuScene(activity.getVertexBufferObjectManager(), this));
		}
	}

    /**
     * Tutorial functionality
     */

    private void initTutorialPickup()
    {
        tutorialPickup = true;

        scoreLabel.setVisible(false);
        pauseButton.setVisible(false);

		String text = activity.getString(R.string.tutorial_grabber);

		tutorialText.setText(text);
		tutorialText.setX((ScreenSize.getScreenWidth() / 2) - (tutorialText.getWidth() / 2));
		tutorialText.setY(grabber.getY() - BlockGrabber.BLOCK_MARGIN - tutorialText.getHeight());
		tutorialText.setVisible(true);
    }

	private void initTutorialMatch()
	{
		tutorialPickup = false;
		tutorialMatch = true;

		String text = activity.getString(R.string.tutorial_match);
		tutorialText.setText(text);
		tutorialText.setX((ScreenSize.getScreenWidth() / 2) - (tutorialText.getWidth() / 2));
		tutorialText.setY((BlockGrabber.BLOCK_MARGIN + grabber.getBlockSize()) * 4 + ScreenSize.getStatusBarSize() + BlockGrabber.BLOCK_MARGIN);
	}

    private void completeTutorialMatching()
    {
		tutorialMatch = false;

		scoreLabel.setVisible(true);
		pauseButton.setVisible(true);

		tutorialText.setVisible(false);

		hasTutorialBoard = false;
		SharedPreferences.Editor e = sharedPreferences.edit();
		e.putBoolean(activity.getString(R.string.shdprf_tutorial_board), true);
		e.apply();
    }

	private boolean evalTutorialBlock(BlockContainer container)
	{
		String text;

		if (!hasTutorialBlocker && container.getBlock() instanceof BlockBlocker)
		{
			text = activity.getString(R.string.tutorial_blocker);
		}
		else if (!hasTutorialBomb && container.getBlock() instanceof BlockBomb)
		{
			text = activity.getString(R.string.tutorial_bomb);
		}
		else if (!hasTutorialPaint && container.getBlock() instanceof BlockPaint)
		{
			text = activity.getString(R.string.tutorial_paint);
		}
		else if (!hasTutorialWildcard && container.getBlock() instanceof BlockWildcard)
		{
			text = activity.getString(R.string.tutorial_wildcard);
		}
		else
		{
			return false;
		}

		tutorialText.setText(text);
		tutorialText.setX((ScreenSize.getScreenWidth() / 2) - (tutorialText.getWidth() / 2));
		tutorialText.setY(scoreMiddle - (tutorialText.getHeight() / 2.0f));

		pauseButton.setVisible(false);
		scoreLabel.setVisible(false);
		tutorialText.setVisible(true);

		tutorialBlock = true;
		tutorialBlockType = container.getBlock().getClass();

		return true;
	}

	private void completeTutorialBlockFade()
	{
		for (int i = 0; i < 4; i++)
			grabber.getContainer(i).setFaded(false);
	}

	private void completeTutorialBlock()
	{
		tutorialText.setVisible(false);
		scoreLabel.setVisible(true);
		pauseButton.setVisible(true);

		String pref = null;
		if (tutorialBlockType == BlockBlocker.class)
		{
			pref = activity.getString(R.string.shdprf_tutorial_blocker);
			hasTutorialBlocker = true;
		}
		else if (tutorialBlockType == BlockBomb.class)
		{
			pref = activity.getString(R.string.shdprf_tutorial_bomb);
			hasTutorialBomb = true;
		}
		else if (tutorialBlockType == BlockPaint.class)
		{
			pref = activity.getString(R.string.shdprf_tutorial_paint);
			hasTutorialPaint = true;
		}
		else if (tutorialBlockType == BlockWildcard.class)
		{
			pref = activity.getString(R.string.shdprf_tutorial_wildcard);
			hasTutorialWildcard = true;
		}

		if (pref != null)
		{
			SharedPreferences.Editor e = sharedPreferences.edit();
			e.putBoolean(pref, true);
			e.apply();
		}

		tutorialBlock = false;
		tutorialBlockType = null;
	}
}
