package me.zachcheatham.blockdown.game;

import java.util.ArrayList;
import java.util.List;

import me.zachcheatham.blockdown.game.block.Block;
import me.zachcheatham.blockdown.game.block.BlockBasic;
import me.zachcheatham.blockdown.game.block.BlockBlocker;
import me.zachcheatham.blockdown.game.block.BlockWildcard;
import me.zachcheatham.blockdown.scene.BoardScene;
import me.zachcheatham.blockdown.util.ScreenSize;

import org.andengine.entity.Entity;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

public class BlockBoard extends Entity
{
	private static final float BLOCK_MARGIN = 10.0f;
	
	private final BlockContainer[] spaces = new BlockContainer[16];
	private final List<BlockContainer> clearQueue = new ArrayList<BlockContainer>();
	
	public BlockBoard(BoardScene scene, VertexBufferObjectManager vbom)
	{
		// Setup
		this.setX(0.0f);
		this.setY(0.0f);
		this.setColor(Color.BLUE);
		
		// Create spaces
		float blockSize = (ScreenSize.getScreenWidth() - BLOCK_MARGIN * 5) / 4;
		
		for (int i = 0; i < 16; i++)
		{
			int row = (int) Math.floor(i / 4);
			int col = i % 4;
			
			float gapY = row * BLOCK_MARGIN;
			float gapX = col * BLOCK_MARGIN;
			
			float x = (float) Math.floor(BLOCK_MARGIN + col * blockSize + gapX);
			float y = (float) Math.floor(ScreenSize.getStatusBarSize() + BLOCK_MARGIN + row * blockSize + gapY);
			
			BlockContainer b = new BlockContainer(scene,
													BlockLocation.BlockBoard,
													i,
													x,
													y,
													blockSize,
													blockSize,
													vbom);
			
			spaces[i] = b;
			
			this.attachChild(b);
			scene.registerTouchArea(b);
		}
	}
	
	public int checkMatches()
	{
		int points = 0;
		
		// Rows
		if (matchGroup(new BlockContainer[]{spaces[0], spaces[1], spaces[2], spaces[3]})) points++;
		if (matchGroup(new BlockContainer[]{spaces[4], spaces[5], spaces[6], spaces[7]})) points++;
		if (matchGroup(new BlockContainer[]{spaces[8], spaces[9], spaces[10], spaces[11]})) {points++;}
		if (matchGroup(new BlockContainer[]{spaces[12], spaces[13], spaces[14], spaces[15]})) {points++;}
		
		// Columns
		if (matchGroup(new BlockContainer[]{spaces[0], spaces[4], spaces[8], spaces[12]})) {points++;}
		if (matchGroup(new BlockContainer[]{spaces[1], spaces[5], spaces[9], spaces[13]})) {points++;}
		if (matchGroup(new BlockContainer[]{spaces[2], spaces[6], spaces[10], spaces[14]})) {points++;}
		if (matchGroup(new BlockContainer[]{spaces[3], spaces[7], spaces[11], spaces[15]})) {points++;}
		
		// Diagonals
		if (matchGroup(new BlockContainer[]{spaces[0], spaces[5], spaces[10], spaces[15]})) {points++;}
		if (matchGroup(new BlockContainer[]{spaces[3], spaces[6], spaces[9], spaces[12]})) {points++;}
		
		// Clear everything we matched
		for (BlockContainer space : clearQueue)
			space.flashClearBlock();
		
		clearQueue.clear();
		
		return points;
	}
	
	public boolean isFull()
	{
		for (BlockContainer space : spaces)
			if (space.isEmpty()) return false;
		
		return true;
	}
	
	public void clear()
	{
		for (BlockContainer space : spaces)
			space.clearBlock(false);
	}
	
	private boolean matchGroup(BlockContainer[] group)
	{	
		Block challenge = null;
				
		for (int i = 0; i < group.length; i++)
		{			
			if (group[i].isEmpty())
				return false;
			else if (group[i].getBlock() instanceof BlockBasic)
			{
				challenge = group[i].getBlock();
				break;
			}
			
			if (i == (group.length - 1) && group[i].getBlock() instanceof BlockWildcard)
				challenge = group[i].getBlock();
		}
		
		if (challenge == null)
			return false;
				
		for (int i = 0; i < group.length; i++)
		{
			if (group[i].getBlock() == challenge)
				continue;
			
			if (group[i].isEmpty())
				return false;
						
			if (!group[i].getBlock().matches(challenge))
				return false;
		}
				
		// All blocks matched the first block, we add them to the clear queue
		for (BlockContainer space : group)
			if (!clearQueue.contains(space))
				clearQueue.add(space);
		
		// Delete blockers with this color
		for (BlockContainer space : spaces)
		{
			if (space.getBlock() instanceof BlockBlocker)
			{
				if (space.getBlock().getColorId() == challenge.getColorId())
				{
					space.flashClearBlock();
				}
			}
		}
		
		return true;
	}

	public void setBlockAt(int i, Block block) 
	{
		spaces[i].setBlock(block);
	}
}
