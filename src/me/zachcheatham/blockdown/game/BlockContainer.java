package me.zachcheatham.blockdown.game;

import me.zachcheatham.blockdown.animation.AnimationLength;
import me.zachcheatham.blockdown.game.block.Block;
import me.zachcheatham.blockdown.resources.Textures;

import me.zachcheatham.blockdown.scene.BoardScene;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.AnimatedSprite.IAnimationListener;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

public class BlockContainer extends TiledSprite
{
	private final BoardScene scene;
	private Block block = null;
	public final BlockLocation blockLocation;
	public final int id;
	
	public BlockContainer(BoardScene scene, BlockLocation location, int id, float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager vbom)
	{
		super(pX, pY, pWidth, pHeight, Textures.blocks, vbom);
		
		this.scene = scene;
		this.id = id;
		this.blockLocation = location;
	}
	
	@Override
    public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY)
	{
		if (pSceneTouchEvent.isActionDown() || pSceneTouchEvent.isActionUp())
		{
			scene.onContainerTouched(this, pSceneTouchEvent);
		}
		
		return pSceneTouchEvent.isActionUp();
    }
	
	public void setBlock(Block block)
	{		
		this.block = block;
		setCurrentTileIndex(block.getTileIndex());
	}
	
	public void clearBlock(boolean keepSprite)
	{
		block = null;
        if (!keepSprite)
		    setCurrentTileIndex(0);
	}

    public void syncSprite()
    {
        if (block == null)
            setCurrentTileIndex(0);
        else
            setCurrentTileIndex(block.getTileIndex());
    }

	public Block getBlock()
	{
		return block;
	}
	
	public boolean isEmpty()
	{
		return block == null;
	}

	public void setFaded(boolean faded)
	{
		if (faded)
			this.setAlpha(0.25f);
		else
			this.setAlpha(1.0f);
	}
	
	public void flashClearBlock()
	{
        clearBlock(true);

		final AnimatedSprite flash = new AnimatedSprite(0, 0, getWidth(), getHeight(), Textures.blockFlash, getVertexBufferObjectManager());
		attachChild(flash);
		flash.animate(AnimationLength.flash, false, new IAnimationListener()
		{

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {}

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite)
			{
                syncSprite();
                detachChild(flash);
			}
		});
	}
	
	public void explosionClearBlock()
	{
        clearBlock(true);

		final AnimatedSprite explosion = new AnimatedSprite(0, 0, getWidth(), getHeight(), Textures.blockExplosion, getVertexBufferObjectManager());
		attachChild(explosion);
		explosion.animate(AnimationLength.explosion, false, new IAnimationListener()
		{

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount)
			{
			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex)
			{
				if (pNewFrameIndex == 7)
					syncSprite();
			}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount)
			{
			}

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite)
			{
				detachChild(explosion);
			}
		});
	}

	public void paintTo(Block block)
	{
		final TiledSprite fadeOutSprite = new TiledSprite(0, 0, getWidth(), getHeight(), Textures.blocks, getVertexBufferObjectManager());
		
		fadeOutSprite.setCurrentTileIndex(getCurrentTileIndex());
		attachChild(fadeOutSprite);
		setBlock(block);
		
		fadeOutSprite.registerEntityModifier(new AlphaModifier(0.326f, 1.0f, 0.0f, new IEntityModifierListener()
		{
			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem)
			{
			}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem)
			{
				pItem.detachSelf();
			}
		}));
	}
}
