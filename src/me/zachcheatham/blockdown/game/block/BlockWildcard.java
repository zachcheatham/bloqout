package me.zachcheatham.blockdown.game.block;

public class BlockWildcard extends Block
{
	@Override
	public int getColorId()
	{
		return -1;
	}
	
	@Override
	public int getTileIndex()
	{
		return 7;
	}
	
	@Override
	public boolean matches(Block block)
	{
		return true;
	}
}
