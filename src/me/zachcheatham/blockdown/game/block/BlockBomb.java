package me.zachcheatham.blockdown.game.block;

import me.zachcheatham.blockdown.game.BlockContainer;
import me.zachcheatham.blockdown.resources.Sounds;

public class BlockBomb extends Block
{
	@Override
	public int getTileIndex()
	{
		return 6;
	}
	
	@Override
	public boolean isValidTarget(Block block)
	{
		return true;
	}
	
	@Override
	public void dropOnto(BlockContainer space)
	{
		space.explosionClearBlock();
		Sounds.playExplosion();
	}

	@Override
	public int getColorId()
	{
		return -1;
	}
}
