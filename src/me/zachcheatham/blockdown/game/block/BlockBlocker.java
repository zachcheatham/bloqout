package me.zachcheatham.blockdown.game.block;

import java.util.Random;

public class BlockBlocker extends Block
{
	private final int colorId;
	
	public BlockBlocker()
	{
		colorId = new Random().nextInt(5);
	}
	
	public BlockBlocker(int colorId)
	{
		this.colorId = colorId;
	}

	@Override
	public int getTileIndex()
	{
		return 8 + colorId;
	}

	@Override
	public boolean matches(Block b)
	{
		return false;
	}
	
	@Override
	public int getColorId()
	{
		return colorId;
	}
}
