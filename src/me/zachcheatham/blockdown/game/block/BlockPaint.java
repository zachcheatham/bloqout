package me.zachcheatham.blockdown.game.block;

import java.util.Random;

import me.zachcheatham.blockdown.game.BlockContainer;
import me.zachcheatham.blockdown.resources.Sounds;

public class BlockPaint extends Block
{
	private final int colorId;
	
	public BlockPaint()
	{
		colorId = new Random().nextInt(5);
	}
	
	@Override
	public int getColorId()
	{
		return colorId;
	}

	@Override
	public boolean matches(Block b)
	{
		return false;
	}
	
	@Override
	public boolean isValidTarget(Block block)
	{
		return (block == null || block instanceof BlockBasic || block instanceof BlockBlocker);
	}
	
	@Override
	public void dropOnto(BlockContainer space)
	{
		if (space.getBlock() != null)
			if (space.getBlock() instanceof BlockBasic)
				space.paintTo(new BlockBasic(colorId));
			else if (space.getBlock() instanceof BlockBlocker)
				space.paintTo(new BlockBlocker(colorId));
		
		Sounds.playPaint();
	}
	
	@Override
	public int getTileIndex()
	{
		return 13 + colorId;
	}

}
