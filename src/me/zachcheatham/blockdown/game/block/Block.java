package me.zachcheatham.blockdown.game.block;

import me.zachcheatham.blockdown.game.BlockContainer;

public abstract class Block
{	
	public abstract int getColorId();
	public abstract int getTileIndex();
	
	public void dropOnto(BlockContainer space)
	{
		space.setBlock(this);
	}
	
	public boolean isValidTarget(Block block)
	{
		return (block == null);
	}
	
	public boolean matches(Block block)
	{	
		return (block.getColorId() == getColorId());
	}
}
