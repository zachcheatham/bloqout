package me.zachcheatham.blockdown.game.block;

import java.util.Random;

public class BlockBasic extends Block
{
	private final int colorId;
	
	public BlockBasic()
	{
		colorId = new Random().nextInt(5);
	}
	
	public BlockBasic(int colorId)
	{
		this.colorId = colorId;
	}

	@Override
	public int getTileIndex()
	{
		return 1 + colorId;
	}

	@Override
	public int getColorId()
	{
		return colorId;
	}
}
