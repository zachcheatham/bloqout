package me.zachcheatham.blockdown.game;

import me.zachcheatham.blockdown.animation.GameoverAlertModifier;
import me.zachcheatham.blockdown.resources.Fonts;
import me.zachcheatham.blockdown.resources.Sounds;
import me.zachcheatham.blockdown.resources.Textures;
import me.zachcheatham.blockdown.util.ButtonFactory;
import me.zachcheatham.blockdown.util.ScreenSize;
import me.zachcheatham.blockdown.util.Tag;

import me.zachcheatham.blockdown.util.game.GameHelper;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;

public class GameOverScene extends Scene
{
	private final GameScene gameScene;
	private final Text scoreLabel;
	private final Text highScoreLabel;
	private final Sprite scoreBackground;
	private final ButtonSprite restartButton;
	private final ButtonSprite leaderboardButton;
	private final int score;
	private final int oldHighScore;
	
	public GameOverScene(int score, int oldHighScore, GameHelper gameHelper, GameScene gameScene, VertexBufferObjectManager vbom, OnClickListener clickListener)
	{
		this.gameScene = gameScene;
		
		this.oldHighScore = oldHighScore;
		this.score = score;
		
		setBackgroundEnabled(false);
		setBackground(new Background(0.0f, 0.0f, 0.0f, 0.5f));
		setTouchAreaBindingOnActionDownEnabled(true);
		
		scoreBackground = new Sprite(0, 0, Textures.dialog, vbom);
		ScreenSize.scaleSprite(scoreBackground);
		scoreBackground.setX((ScreenSize.getScreenWidth() / 2) - (scoreBackground.getWidth() / 2));
		scoreBackground.setY((ScreenSize.getScreenHeight() / 2) - (scoreBackground.getHeight() / 2));
		
		scoreBackground.setVisible(false);
		attachChild(scoreBackground);
		
		Text scoreTextLabel = new Text(32 * ScreenSize.getDensity(), (scoreBackground.getHeight() / 3.5f) - (Fonts.buttonFont.getLineHeight() / 2), Fonts.buttonFont, "Score", new TextOptions(HorizontalAlign.LEFT), vbom);
		scoreBackground.attachChild(scoreTextLabel);
		
		Text highScoreTextLabel = new Text(32 * ScreenSize.getDensity(), (scoreBackground.getHeight() * 0.75f) - (Fonts.buttonFont.getLineHeight() / 2), Fonts.buttonFont, "High Score", new TextOptions(HorizontalAlign.LEFT), vbom);
		scoreBackground.attachChild(highScoreTextLabel);
		
		scoreLabel = new Text(0, (scoreBackground.getHeight() / 3.5f) - (Fonts.buttonFont.getLineHeight() / 2), Fonts.buttonFont, "0", 3, new TextOptions(HorizontalAlign.RIGHT), vbom);
		scoreLabel.setX(scoreBackground.getWidth() - scoreLabel.getWidth() - 32 * ScreenSize.getDensity());
		scoreBackground.attachChild(scoreLabel);
		
		highScoreLabel = new Text(0, (scoreBackground.getHeight() * 0.75f) - (Fonts.buttonFont.getLineHeight() / 2), Fonts.buttonFont, oldHighScore + "", 3, new TextOptions(HorizontalAlign.RIGHT), vbom);
		highScoreLabel.setX(scoreBackground.getWidth() - highScoreLabel.getWidth() - 32 * ScreenSize.getDensity());
		scoreBackground.attachChild(highScoreLabel);
		
		restartButton = ButtonFactory.createFullButton("Restart", Tag.BUTTON_RESTART, vbom, clickListener);
		restartButton.setVisible(false);
		restartButton.setY(ScreenSize.getScreenHeight() / 1.5f);
		
		if (gameHelper.isSignedIn())
		{			
			leaderboardButton = ButtonFactory.createFullButton("Leaderboard", Tag.BUTTON_LEADERBOARD, vbom, clickListener);
			leaderboardButton.setVisible(false);
			leaderboardButton.setY(ScreenSize.getScreenHeight() / 1.5f + leaderboardButton.getHeight());
			
			registerTouchArea(leaderboardButton);
			attachChild(leaderboardButton);
		}
		else
		{
			leaderboardButton = null;
		}
		
		registerTouchArea(restartButton);
		attachChild(restartButton);
		
		Sprite gameover = new Sprite(0, 0, Textures.gameover, vbom);
		ScreenSize.scaleSprite(gameover);
		gameover.setX((ScreenSize.getScreenWidth() / 2) - (gameover.getWidth() / 2));
		gameover.setY((ScreenSize.getScreenHeight() / 4) - (gameover.getHeight() / 2));
		attachChild(gameover);
		
		// Animate;
		final GameOverScene inst = this;
		this.registerUpdateHandler(new TimerHandler(0.8f, new ITimerCallback()
		{
			@Override
			public void onTimePassed(TimerHandler pTimerHandler)
			{
				inst.setBackgroundEnabled(true);
				inst.getScoreBackground().setVisible(true);
				
				inst.registerEntityModifier(new GameoverAlertModifier(1, 0, inst.getScore()));
			}
		}));
		
		// Play Sound
		Sounds.playLose();
	}

	public void showButtons()
	{
		restartButton.setVisible(true);
		
		if (leaderboardButton != null)
			leaderboardButton.setVisible(true);
	}
	
	public void submitScore()
	{
		gameScene.submitScore();
	}
	
	public int getScore()
	{
		return score;
	}
	
	public int getOldHighScore()
	{
		return oldHighScore;
	}

	public Sprite getScoreBackground()
	{
		return scoreBackground;
	}
	
	public Text getScoreLabel()
	{
		return scoreLabel;
	}
	
	public Text getHighScoreLabel()
	{
		return highScoreLabel;
	}
}
