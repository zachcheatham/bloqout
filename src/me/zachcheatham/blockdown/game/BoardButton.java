package me.zachcheatham.blockdown.game;


import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class BoardButton extends ButtonSprite
{
	private final GameScene scene;
	
	public BoardButton(float pX, float pY, ITextureRegion pNormalTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, OnClickListener pOnClickListener, GameScene scene)
	{
		super(pX, pY, pNormalTextureRegion, pVertexBufferObjectManager, pOnClickListener);
		
		this.scene = scene;
	}
	
	@Override
	public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY)
	{
		super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
		
		if (scene.isMovingBlock())
			return false;
		
		return true;
	}
}
