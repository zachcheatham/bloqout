package me.zachcheatham.blockdown.game;

import java.util.Random;

import me.zachcheatham.blockdown.game.block.BlockBasic;
import me.zachcheatham.blockdown.game.block.BlockBlocker;
import me.zachcheatham.blockdown.game.block.BlockBomb;
import me.zachcheatham.blockdown.game.block.BlockPaint;
import me.zachcheatham.blockdown.game.block.BlockWildcard;
import me.zachcheatham.blockdown.scene.BoardScene;
import me.zachcheatham.blockdown.util.ScreenSize;

import org.andengine.entity.Entity;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

public class BlockGrabber extends Entity
{
	public static final float BLOCK_MARGIN = 10.0f;
	
	private final BlockContainer[] spaces = new BlockContainer[4];
	private final Random randomizer = new Random();
	private float blockSize;
	
	public BlockGrabber(BoardScene scene, VertexBufferObjectManager vbom)
	{
		// Setup
		this.setX(0.0f);
		this.setY(0.0f);
		this.setColor(Color.BLUE);

		// Metrics
		blockSize = (ScreenSize.getScreenWidth() - BLOCK_MARGIN * 5) / 4;

		// Create spaces
		for (int i = 0; i < 4; i++)
		{
			float gapX = i * BLOCK_MARGIN;
			
			float x = (float) Math.floor(BLOCK_MARGIN + i * blockSize + gapX);
			
			BlockContainer b = new BlockContainer(scene,
													BlockLocation.Grabber,
													i,
													x,
													0,
													blockSize,
													blockSize,
													vbom);
			
			spaces[i] = b;
			
			this.attachChild(b);
			scene.registerTouchArea(b);
		}
	}
	
	public void refill(boolean preventSpecials)
	{
		boolean specialSpawned = false;

		for (int i = 0; i < 4; i++)
		{
			// 1 in 11 chance of power up
			if (!preventSpecials && !specialSpawned && randomizer.nextInt(11) == 10)
			{
				switch (randomizer.nextInt(4))
				{
				case 0:
					spaces[i].setBlock(new BlockBomb());
					break;
				case 1:
					spaces[i].setBlock(new BlockWildcard());
					break;
				case 2:
					spaces[i].setBlock(new BlockBlocker());
					break;
				case 3:
					spaces[i].setBlock(new BlockPaint());
					break;
				}

				specialSpawned = true;
			}
			// 10 in 11 chance of regular block
			else
			{
				spaces[i].setBlock(new BlockBasic());
			}
		}                             
	}
	
	public float getBlockSize()
	{
		return blockSize;
	}

	public boolean isEmpty()
	{
		for (BlockContainer space : spaces)
			if (!space.isEmpty()) return false;
		
		return true;
	}

	public BlockContainer getContainer(int index)
	{
		return spaces[index];
	}

	public boolean containsGameSaver()
	{
		for (BlockContainer space : spaces)
			if (!space.isEmpty() && (space.getBlock() instanceof BlockPaint || space.getBlock() instanceof BlockBomb))
				return true;
		
		return false;
	}
}