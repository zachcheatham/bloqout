package me.zachcheatham.blockdown;

import me.zachcheatham.blockdown.game.GameScene;
import me.zachcheatham.blockdown.game.MenuScene;
import me.zachcheatham.blockdown.resources.Fonts;
import me.zachcheatham.blockdown.resources.Sounds;
import me.zachcheatham.blockdown.resources.Textures;
import me.zachcheatham.blockdown.scene.MainScene;
import me.zachcheatham.blockdown.util.game.GameHelper;
import me.zachcheatham.blockdown.util.game.GameHelper.GameHelperListener;
import me.zachcheatham.blockdown.util.game.Leaderboard;
import me.zachcheatham.blockdown.util.ScreenSize;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.FadeInModifier;
import org.andengine.entity.modifier.FadeOutModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;

import android.content.Intent;
import android.os.Bundle;

public class Blockdown extends SimpleBaseGameActivity implements GameHelperListener
{
	private GameHelper gameHelper;
	
	@Override
	public void onCreate(Bundle pSavedInstanceState)
	{
		ScreenSize.calculate(this);
		
		gameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
		gameHelper.enableDebugLog(true);
		gameHelper.setup(this);

		super.onCreate(pSavedInstanceState);
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		gameHelper.onStart(this);
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();
		gameHelper.onStop();
	}
	
	@Override
	protected void onActivityResult(int request, int response, Intent data)
	{
	    super.onActivityResult(request, response, data);
	    gameHelper.onActivityResult(request, response, data);
	}
	
	@Override
	public void onBackPressed()
	{
		Scene scene = getEngine().getScene();
		
		if (scene instanceof GameScene)
		{
			if (scene.hasChildScene() && scene.getChildScene() instanceof MenuScene)
			{
				scene.clearChildScene();
			}
			else
			{
				changeScene(Scenes.MAIN);
			}
		}
		else if (scene instanceof MainScene)
		{
			finish();
		}
	}
	
	// ===========================================================
	// Andengine
	// ===========================================================
	
	@Override
	public EngineOptions onCreateEngineOptions()
	{		
    	// Complete the size of the surface
		final Camera camera = new Camera(0, 0, ScreenSize.getScreenWidth(), ScreenSize.getScreenHeight());
        EngineOptions options = new EngineOptions(false, ScreenOrientation.PORTRAIT_FIXED, new FillResolutionPolicy(), camera);
        
        options.getAudioOptions().setNeedsSound(true);
        
        return options;
	}
	
	@Override 
	protected void onCreateResources()
	{
		// Load fonts
		Fonts.loadFonts(this);
		
		// Load textures
		Textures.loadTextures(getTextureManager(), getAssets(), getVertexBufferObjectManager());
		
		// Load sounds
		Sounds.loadSounds(getSoundManager(), this);
	}

	@Override
	protected Scene onCreateScene()                                                                                                                                            
	{
		return new MainScene(this);
	}
	
	// ===========================================================
	// Public Methods
	// ===========================================================
	
	public void changeScene(Scenes scene)
	{
		final Scene oldScene = getEngine().getScene();
		final Scene newScene;
		switch (scene)
		{
		case MAIN:
			newScene = new MainScene(this);
			break;
		case GAME:
			newScene = new GameScene(this);
			break;
		default:
			newScene = null;
		}
		
		final Rectangle fade = new Rectangle(0, 0, ScreenSize.getScreenWidth(), ScreenSize.getScreenHeight(), getEngine().getVertexBufferObjectManager());
		fade.setColor(Color.BLACK);
		fade.setAlpha(0.0f);
		oldScene.attachChild(fade);
		
		fade.registerEntityModifier(new FadeInModifier(0.15f, new IEntityModifierListener()
		{
			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem)
			{
				oldScene.detachChild(fade);
				newScene.attachChild(fade);
				getEngine().setScene(newScene);
				fade.registerEntityModifier(new FadeOutModifier(0.15f, new IEntityModifierListener()
				{
					@Override
					public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}

					@Override
					public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem)
					{
						newScene.detachChild(fade);
					}	
				}));
			}
			
		}));
	}
	
	public GameHelper getGoogleGameHelper()
	{
		return gameHelper;
	}

	// ===========================================================
	// GameHelper Listener
	// ===========================================================
	
	@Override
	public void onSignInFailed()
	{
		if (getEngine().getScene() instanceof MainScene)
			((MainScene) getEngine().getScene()).setGSStatus(false);
	}

	@Override
	public void onSignInSucceeded()
	{
		Leaderboard.fetchHighScores(gameHelper.getApiClient());
		
		if (getEngine().getScene() instanceof MainScene)
			((MainScene) getEngine().getScene()).setGSStatus(true);
	}
}
