package me.zachcheatham.blockdown.resources;

import me.zachcheatham.blockdown.util.ScreenSize;

import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.font.FontManager;
import org.andengine.opengl.font.StrokeFont;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.color.Color;

import android.graphics.Typeface;
import android.util.Log;

public class Fonts
{
	public static Font scoreFont;
	public static Font buttonFont;
	public static Font tutorialFont;
	
	public static void loadFonts(BaseGameActivity game)
	{
		scoreFont = createFont(game.getFontManager(), game.getTextureManager(), Typeface.createFromAsset(game.getAssets(), "fonts/Bitwise.ttf"), (int) Math.floor((ScreenSize.getScreenWidth() / 4) / 2), Color.WHITE_ABGR_PACKED_INT, false);
		buttonFont = createStrokeFont(game.getFontManager(), game.getTextureManager(), Typeface.createFromAsset(game.getAssets(), "fonts/Bitwise.ttf"), 36, Color.WHITE_ABGR_PACKED_INT, ScreenSize.getDensity(), Color.BLACK_ABGR_PACKED_INT, true);
		tutorialFont = createFont(game.getFontManager(), game.getTextureManager(), Typeface.createFromAsset(game.getAssets(), "fonts/Bitwise.ttf"), (int) Math.floor(ScreenSize.getScreenWidth() / 12), Color.WHITE_ABGR_PACKED_INT, false);

		Log.i("Blockdown", "Fonts loaded.");
	}
	
	private static StrokeFont createStrokeFont(FontManager fm, TextureManager tm, Typeface face, int size, int color, float strokeWidth, int strokeColor, boolean calculateDensity)
	{
		if (calculateDensity)
			size *= ScreenSize.getDensity();
		
		BitmapTextureAtlas texture = new BitmapTextureAtlas(tm, 512, 512, TextureOptions.BILINEAR);
		
		StrokeFont font = FontFactory.createStroke(fm, texture, face, size, true, color, strokeWidth, strokeColor);
		font.load();
		
		return font;
	}
	
	private static Font createFont(FontManager fm, TextureManager tm, Typeface face, int size, int color, boolean calculateDensity)
	{
		if (calculateDensity)
			size *= ScreenSize.getDensity();
		
		Font font = FontFactory.create(fm, tm, 512, 512, face, size, color);
		font.load();
		
		return font;
	}
}
