package me.zachcheatham.blockdown.resources;

import me.zachcheatham.blockdown.util.ScreenSize;

import org.andengine.entity.scene.background.RepeatingSpriteBackground;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.source.AssetBitmapTextureAtlasSource;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.res.AssetManager;
import android.util.Log;

public class Textures
{
	public static TextureRegion			dialog;
	public static TextureRegion 		logo;
	public static TextureRegion			gameover;
	public static TextureRegion			paused;
	
	public static RepeatingSpriteBackground background;
	
	public static ITiledTextureRegion 	buttonFull;
	public static ITiledTextureRegion	buttonHalf;
	public static ITiledTextureRegion	buttonSquare;
	public static ITiledTextureRegion	buttonSignIn;
	public static ITiledTextureRegion	buttonSignOut;
	public static ITiledTextureRegion	buttonPause;
	public static ITiledTextureRegion 	blocks;
	public static ITiledTextureRegion 	blockFlash;
	public static ITiledTextureRegion 	blockExplosion;
	
	public static void loadTextures(TextureManager textureManager, AssetManager assetManager, VertexBufferObjectManager vbom)
	{
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		
		dialog = createTexture(textureManager, assetManager, "dialog.png", 1016, 416);
		logo = createTexture(textureManager, assetManager, "logo.png", 1016, 142);
		gameover = createTexture(textureManager, assetManager, "gameover.png", 1014, 142);
		paused = createTexture(textureManager, assetManager, "paused.png", 646, 142);
		
		background = createRepeatingBackground(textureManager, assetManager, vbom, "background.png");
		
		blocks			= createTiledTexture(textureManager, assetManager, "blocks.png", 					2160,	810, 	8, 3);
		buttonSquare	= createTiledTexture(textureManager, assetManager, "button_square.png", 			216,	432, 	1, 2);
		buttonHalf		= createTiledTexture(textureManager, assetManager, "button_half.png",				516,	232,	1, 2);
		buttonFull 		= createTiledTexture(textureManager, assetManager, "button_full.png", 				1016,	432, 	1, 2);
		buttonSignIn	= createTiledTexture(textureManager, assetManager, "button_signin.png",				280,	224,	1, 2);
		buttonSignOut	= createTiledTexture(textureManager, assetManager, "button_signout.png",			280,	224,	1, 2);
		buttonPause		= createTiledTexture(textureManager, assetManager, "pause.png", 					72,		144,	1, 2);
		blockFlash 		= createTiledTexture(textureManager, assetManager, "animation_block_flash.png", 	810,	810, 	3, 3);
		blockExplosion	= createTiledTexture(textureManager, assetManager, "animation_block_explosion.png", 1000,	1000, 	4, 4);
		
		Log.i("Blockdown", "Textures loaded.");
	}
	
	private static RepeatingSpriteBackground createRepeatingBackground(TextureManager textureManager, AssetManager assetManager, VertexBufferObjectManager vbom, String file)
	{
		AssetBitmapTextureAtlasSource atlas = AssetBitmapTextureAtlasSource.create(assetManager, "gfx/" + file);
		return new RepeatingSpriteBackground(ScreenSize.getScreenWidth(), ScreenSize.getScreenHeight(), textureManager, atlas, vbom);
	}
	
	private static TextureRegion createTexture(TextureManager  textureManager, AssetManager assetManager, String file, int w, int h)
	{
		BitmapTextureAtlas atlas = new BitmapTextureAtlas(textureManager, w, h);
		TextureRegion textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(atlas, assetManager, file, 0, 0);
		atlas.load();
		
		return textureRegion;
	}
	
	private static TiledTextureRegion createTiledTexture(TextureManager textureManager, AssetManager assetManager, String file, int w, int h, int cols, int rows)
	{
		BitmapTextureAtlas atlas = new BitmapTextureAtlas(textureManager, w, h);
		TiledTextureRegion tiledTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(atlas, assetManager, file, 0, 0, cols, rows);
		atlas.load();
		
		return tiledTextureRegion;
	}
}
