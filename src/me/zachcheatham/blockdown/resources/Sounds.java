package me.zachcheatham.blockdown.resources;

import java.io.IOException;
import java.util.Random;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.audio.sound.SoundManager;

import android.content.Context;
import android.util.Log;

public class Sounds
{
	private static Random random;
	
	private static Sound match0;
	private static Sound match1;
	private static Sound match2;
	private static Sound match3;
	
	private static Sound explosion0;
	private static Sound explosion1;
	
	private static Sound lose0;
	private static Sound lose1;
	private static Sound lose2;
	
	private static Sound paint0;
	private static Sound paint1;
	
	public static void loadSounds(SoundManager sm, Context c)
	{
		random = new Random();
		
		try
		{
			match0 = SoundFactory.createSoundFromAsset(sm, c, "snd/Match0.wav");
			match1 = SoundFactory.createSoundFromAsset(sm, c, "snd/Match1.wav");
			match2 = SoundFactory.createSoundFromAsset(sm, c, "snd/Match2.wav");
			match3 = SoundFactory.createSoundFromAsset(sm, c, "snd/Match3.wav");
			
			explosion0 = SoundFactory.createSoundFromAsset(sm, c, "snd/Explosion0.wav");
			explosion1 = SoundFactory.createSoundFromAsset(sm, c, "snd/Explosion1.wav");
			
			lose0 = SoundFactory.createSoundFromAsset(sm, c, "snd/Lose0.wav");
			lose1 = SoundFactory.createSoundFromAsset(sm, c, "snd/Lose1.wav");
			lose2 = SoundFactory.createSoundFromAsset(sm, c, "snd/Lose2.wav");
			
			paint0 = SoundFactory.createSoundFromAsset(sm, c, "snd/Paint0.wav");
			paint1 = SoundFactory.createSoundFromAsset(sm, c, "snd/Paint1.wav");
			
			Log.i("Blockdown", "Sounds loaded.");
		}
		catch (IOException e)
		{
			Log.e("Blockdown", "Sounds failed to load!", e);
		}
	}
	
	public static void playMatch()
	{
		switch (random.nextInt(3))
		{
		case 0:
			match0.play();
			break;
		case 1:
			match1.play();
			break;
		case 2:
			match2.play();
			break;
		case 3:
			match3.play();
			break;
		}
	}
	
	public static void playExplosion()
	{
		switch (random.nextInt(1))
		{
		case 0:
			explosion0.play();
			break;
		case 1:
			explosion1.play();
			break;
		}
	}
	
	public static void playLose()
	{
		switch (random.nextInt(2))
		{
		case 0:
			lose0.play();
			break;
		case 1:
			lose1.play();
			break;
		case 2:
			lose2.play();
			break;
		}
	}

	public static void playPaint()
	{
		switch (random.nextInt(1))
		{
		case 0:
			paint0.play();
			break;
		case 1:
			paint1.play();
			break;
		}
	}
}
