package me.zachcheatham.blockdown.scene;

import me.zachcheatham.blockdown.Blockdown;
import me.zachcheatham.blockdown.game.*;
import me.zachcheatham.blockdown.resources.Textures;
import me.zachcheatham.blockdown.util.ScreenSize;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;

public abstract class BoardScene extends Scene implements IOnSceneTouchListener
{
	protected final Blockdown activity;
	protected final BlockBoard board;
	protected final BlockGrabber grabber;
	protected final BlockContainer movingContainer;
	protected BlockContainer lastSelectedSpace = null;

	public BoardScene(Blockdown activity)
	{
		setBackground(Textures.background);
		setTouchAreaBindingOnActionDownEnabled(true);
		setTouchAreaBindingOnActionMoveEnabled(true);
		setOnSceneTouchListener(this);

		this.activity = activity;

		board = new BlockBoard(this, activity.getVertexBufferObjectManager());
		attachChild(board);

		grabber = new BlockGrabber(this, activity.getVertexBufferObjectManager());
		grabber.setY(ScreenSize.getScreenHeight() - ScreenSize.getActionBarSize() - BlockGrabber.BLOCK_MARGIN - grabber.getBlockSize());
		attachChild(grabber);

		// Let the subclass add its elements so the moving container is on top
		createScene();

		movingContainer = new BlockContainer(this, BlockLocation.MovingBlock, 0, 0, 0, grabber.getBlockSize(), grabber.getBlockSize(), activity.getVertexBufferObjectManager());
		movingContainer.setVisible(false);
		attachChild(movingContainer);
	}

	protected abstract void createScene();
	protected abstract void onBoardUpdate();

	public void onContainerTouched(BlockContainer container, TouchEvent pSceneTouchEvent)
	{
		if (pSceneTouchEvent.isActionDown())
		{
			if (container.blockLocation == BlockLocation.Grabber && !container.isEmpty())
			{
				lastSelectedSpace = container;
				movingContainer.setBlock(container.getBlock());
				container.clearBlock(false);

				float x = pSceneTouchEvent.getX() - (movingContainer.getWidth() / 2);
				float y = pSceneTouchEvent.getY() - (movingContainer.getWidth() / 2);
				movingContainer.setPosition(x, y);
				movingContainer.setVisible(true);
			}
		}
		else if (pSceneTouchEvent.isActionUp())
		{
			if (movingContainer.isVisible())
			{
				if (lastSelectedSpace == container)
				{
					container.setBlock(movingContainer.getBlock());
				}
				else if (container.blockLocation == BlockLocation.BlockBoard && movingContainer.getBlock().isValidTarget(container.getBlock()))
				{
					movingContainer.getBlock().dropOnto(container);
					movingContainer.clearBlock(false);
					lastSelectedSpace = null;
					onBoardUpdate();
				}
				else
				{
					lastSelectedSpace.setBlock(movingContainer.getBlock());
					lastSelectedSpace = null;
				}

				movingContainer.setVisible(false);
				movingContainer.clearBlock(false);
			}
			else if (lastSelectedSpace != null)
			{
				if (container.blockLocation == BlockLocation.BlockBoard)
				{
					if (lastSelectedSpace.getBlock().isValidTarget(container.getBlock()))
					{
						lastSelectedSpace.getBlock().dropOnto(container);
						lastSelectedSpace.clearBlock(false);
						lastSelectedSpace = null;
						onBoardUpdate();
					}
				}
			}
		}
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent)
    {
        if (pSceneTouchEvent.isActionMove() && movingContainer.isVisible())
        {
            float x = pSceneTouchEvent.getX() - (movingContainer.getWidth() / 2);
            float y = pSceneTouchEvent.getY() - (movingContainer.getWidth() / 2);
            movingContainer.setPosition(x, y);

            return true;
        }
        else if (pSceneTouchEvent.isActionUp() && movingContainer.isVisible() && lastSelectedSpace != null)
        {
            movingContainer.setVisible(false);
            lastSelectedSpace.setBlock(movingContainer.getBlock());
            movingContainer.clearBlock(false);

            return true;
        }

        return false;
    }
}
