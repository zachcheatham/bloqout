package me.zachcheatham.blockdown.scene;

import com.google.android.gms.games.Games;
import me.zachcheatham.blockdown.Blockdown;
import me.zachcheatham.blockdown.Scenes;
import me.zachcheatham.blockdown.animation.TiledIndexParticleInitializer;
import me.zachcheatham.blockdown.animation.TiledSpriteParticleSystem;
import me.zachcheatham.blockdown.resources.Textures;
import me.zachcheatham.blockdown.util.ButtonFactory;
import me.zachcheatham.blockdown.util.ScreenSize;
import me.zachcheatham.blockdown.util.Tag;

import me.zachcheatham.blockdown.util.game.Leaderboard;
import org.andengine.entity.particle.emitter.RectangleParticleEmitter;
import org.andengine.entity.particle.initializer.AlphaParticleInitializer;
import org.andengine.entity.particle.initializer.RotationParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.particle.modifier.OffCameraExpireParticleModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;

public class MainScene extends Scene implements OnClickListener
{
	private final Blockdown activity;
	private final ButtonSprite signInButton;
	private final ButtonSprite signOutButton;
	
	public MainScene(Blockdown activity)
	{
		this.activity = activity;
		
		setTouchAreaBindingOnActionDownEnabled(true);
		setTouchAreaBindingOnActionMoveEnabled(true);
		
		setBackground(Textures.background);
		
		RectangleParticleEmitter emitter = new RectangleParticleEmitter(ScreenSize.getScreenWidth() / 2,
                -269,
                ScreenSize.getScreenWidth(), 1);
		TiledSpriteParticleSystem system = new TiledSpriteParticleSystem(emitter,
                1,
                1,
                360,
                Textures.blocks,
                activity.getVertexBufferObjectManager());
	
		system.addParticleInitializer(new AlphaParticleInitializer<TiledSprite>(0.8f));
		system.addParticleInitializer(new VelocityParticleInitializer<TiledSprite>(0, 0, 200, 300));
		system.addParticleInitializer(new RotationParticleInitializer<TiledSprite>(0.0f, 360.0f));
		system.addParticleInitializer(new TiledIndexParticleInitializer(1,4));

		system.addParticleModifier(new OffCameraExpireParticleModifier<TiledSprite>(activity.getEngine().getCamera()));		
		attachChild(system);
		
		Sprite logo = new Sprite(0, 0, Textures.logo, activity.getVertexBufferObjectManager());
		ScreenSize.scaleSprite(logo);
		logo.setX((ScreenSize.getScreenWidth() / 2) - (logo.getWidth() / 2));
		logo.setY((ScreenSize.getScreenHeight() / 4) - (logo.getHeight() / 2));
		
		attachChild(logo);

		ButtonSprite startButton = ButtonFactory.createFullButton("Start Playing",
                Tag.BUTTON_START,
                activity.getVertexBufferObjectManager(),
                this);
        float y = (ScreenSize.getScreenHeight() / 2) - ((startButton.getHeight() * 2) / 2);
		startButton.setY(y);
		registerTouchArea(startButton);
		attachChild(startButton);

        ButtonSprite leaderboardButton = ButtonFactory.createFullButton("Leaderboards",
                Tag.BUTTON_LEADERBOARD, activity.getVertexBufferObjectManager(),
                this);
        leaderboardButton.setY(y + startButton.getHeight());
        registerTouchArea(leaderboardButton);
        attachChild(leaderboardButton);
				
		signInButton = new ButtonSprite(0, 0, Textures.buttonSignIn, activity.getVertexBufferObjectManager(), this);
		signInButton.setTag(Tag.BUTTON_GS);
		signInButton.setVisible(!activity.getGoogleGameHelper().isSignedIn());
		ScreenSize.scaleSprite(signInButton);

		float signY = ScreenSize.getScreenHeight() - ScreenSize.getActionBarSize() - signInButton.getHeight();
		if (ScreenSize.getActionBarSize() == 0) signY -= 10f;
		signInButton.setY(signY);
		signInButton.setX(10f);
		
		signOutButton = new ButtonSprite(0, 0, Textures.buttonSignOut, activity.getVertexBufferObjectManager(), this);
		signOutButton.setTag(Tag.BUTTON_GS);
		signOutButton.setVisible(activity.getGoogleGameHelper().isSignedIn());
		signOutButton.setY(signY);
		signOutButton.setX(10f);
		ScreenSize.scaleSprite(signOutButton);
		
		registerTouchArea(signInButton);
		registerTouchArea(signOutButton);
		attachChild(signInButton);
		attachChild(signOutButton);
	}

	@Override
	public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX, float pTouchAreaLocalY)
	{
		if (pButtonSprite.getTag() == Tag.BUTTON_START)
		{
			activity.changeScene(Scenes.GAME);
		}
        else if (pButtonSprite.getTag() == Tag.BUTTON_LEADERBOARD)
        {
            activity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(activity.getGoogleGameHelper().getApiClient(), Leaderboard.MAIN), 0);
        }
		else if (pButtonSprite.getTag() == Tag.BUTTON_GS)
		{
			if (activity.getGoogleGameHelper().isSignedIn())
			{
				activity.getGoogleGameHelper().signOut();
				setGSStatus(false);
			}
			else
				activity.getGoogleGameHelper().beginUserInitiatedSignIn();
		}
	}
	
	public void setGSStatus(boolean signedIn)
	{
		if (signedIn)
		{
			signOutButton.setVisible(true);
			signInButton.setVisible(false);
		}
		else
		{
			signOutButton.setVisible(false);
			signInButton.setVisible(true);
		}
	}
}