package me.zachcheatham.blockdown.animation;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.SingleValueSpanEntityModifier;
import org.andengine.entity.text.Text;
import org.andengine.util.color.Color;

import me.zachcheatham.blockdown.game.GameOverScene;
import me.zachcheatham.blockdown.util.ScreenSize;

public class GameoverAlertModifier extends SingleValueSpanEntityModifier
{

	public GameoverAlertModifier(float pDuration, float pFromValue, float pToValue)
	{
		super(pDuration, pFromValue, pToValue);
	}
	
	protected GameoverAlertModifier(final GameoverAlertModifier pGameoverAlertModifier)
	{
		super(pGameoverAlertModifier);
	}
	
	@Override
	public GameoverAlertModifier deepCopy()
	{
		return new GameoverAlertModifier(this);
	}

	@Override
	protected void onSetInitialValue(IEntity pItem, float pValue)
	{
		Text scoreLabel = ((GameOverScene) pItem).getScoreLabel();
		
		scoreLabel.setText("" + (int) Math.floor(pValue));
		scoreLabel.setX(((GameOverScene) pItem).getScoreBackground().getWidth() - scoreLabel.getWidth() - 32 * ScreenSize.getDensity());
	}

	@Override
	protected void onSetValue(IEntity pItem, float pPercentageDone, float pValue)
	{
		Text scoreLabel = ((GameOverScene) pItem).getScoreLabel();
		int score = (int) Math.floor(pValue);
		
		scoreLabel.setText("" + score);
		scoreLabel.setX(((GameOverScene) pItem).getScoreBackground().getWidth() - scoreLabel.getWidth() - 32 * ScreenSize.getDensity());
	
		if (score > ((GameOverScene) pItem).getOldHighScore())
		{
			Text highScoreLabel = ((GameOverScene) pItem).getHighScoreLabel();
			
			highScoreLabel.setText(score + "");
			highScoreLabel.setX(scoreLabel.getX());
			highScoreLabel.setColor(Color.YELLOW);
		}
	}
	
	@Override
    protected void onModifierFinished(IEntity pItem)
    {
		super.onModifierFinished(pItem);
		 
		GameOverScene scene  = (GameOverScene) pItem;
		scene.showButtons();
		scene.submitScore();
    }
}
