package me.zachcheatham.blockdown.animation;

import org.andengine.entity.particle.Particle;
import org.andengine.entity.particle.initializer.IParticleInitializer;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.util.math.MathUtils;

public class TiledIndexParticleInitializer implements IParticleInitializer<TiledSprite>
{
	private final int startIndex;
	private final int endIndex;
	
	public TiledIndexParticleInitializer(int startIndex, int endIndex)
	{
		this.startIndex = startIndex;
		this.endIndex = endIndex;
	}

	@Override
	public void onInitializeParticle(Particle<TiledSprite> pParticle)
	{
		pParticle.getEntity().setCurrentTileIndex(MathUtils.random(startIndex, endIndex));
	}
}
