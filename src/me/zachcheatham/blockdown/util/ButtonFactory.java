package me.zachcheatham.blockdown.util;

import me.zachcheatham.blockdown.game.BoardButton;
import me.zachcheatham.blockdown.game.GameScene;
import me.zachcheatham.blockdown.resources.Fonts;
import me.zachcheatham.blockdown.resources.Textures;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;

public class ButtonFactory
{
	public static ButtonSprite createFullButton(String text, int tag, VertexBufferObjectManager vbom, OnClickListener clickListener)
	{		
		ButtonSprite button = new ButtonSprite(0, 0, Textures.buttonFull, vbom, clickListener);
		button.setTag(tag);
		ScreenSize.scaleSprite(button);
		button.setX((ScreenSize.getScreenWidth() / 2) - (button.getWidth() / 2));
		
		Text buttonText = new Text(0, 0, Fonts.buttonFont, text, new TextOptions(HorizontalAlign.CENTER), vbom);
		buttonText.setX((button.getWidth() / 2) - (buttonText.getWidth() / 2));
		buttonText.setY((button.getHeight() / 2) - (buttonText.getHeight() / 2));
		button.attachChild(buttonText);
		
		return button;
	}
	
	public static ButtonSprite createSquareButton(int tag, VertexBufferObjectManager vbom, OnClickListener clickListener, GameScene scene)
	{		
		ButtonSprite button = new BoardButton(0, 0, Textures.buttonSquare, vbom, clickListener, scene);
		button.setTag(tag);
		ScreenSize.scaleSprite(button);
		
		return button;
	}

	public static ButtonSprite createHalfButton(String text, int tag, VertexBufferObjectManager vbom, OnClickListener clickListener)
	{
		ButtonSprite button = new ButtonSprite(0,0, Textures.buttonHalf, vbom, clickListener);
		button.setTag(tag);
		ScreenSize.scaleSprite(button);
		
		Text buttonText = new Text(0, 0, Fonts.buttonFont, text, new TextOptions(HorizontalAlign.CENTER), vbom);
		buttonText.setX((button.getWidth() / 2) - (buttonText.getWidth() / 2));
		buttonText.setY((button.getHeight() / 2) - (buttonText.getHeight() / 2));
		button.attachChild(buttonText);
		
		return button;
	}
}
