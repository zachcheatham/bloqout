package me.zachcheatham.blockdown.util;

public class Tag
{
    public final static int BUTTON_START = 0;
	public final static int BUTTON_RESTART = 1;
	public final static int BUTTON_LEADERBOARD = 2;
	public final static int BUTTON_DEBUG = 3;
	public final static int BUTTON_RESUME = 4;
	public final static int BUTTON_PAUSE = 5;
	public final static int BUTTON_GS = 6;
}
