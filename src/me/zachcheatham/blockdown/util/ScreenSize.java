package me.zachcheatham.blockdown.util;

import org.andengine.entity.sprite.Sprite;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;

public class ScreenSize
{
	private static int screenWidth = 0;
	private static int screenHeight = 0;
	private static int actionHeight = 0;
	private static int statusHeight = 0;
	private static float screenDensity = 0;
	private static float scale = 1.0f;
	
	public static void calculate(Activity a)
	{
		// Screen size
		DisplayMetrics displaymetrics = new DisplayMetrics();
		a.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenWidth = displaymetrics.widthPixels;
		screenHeight = displaymetrics.heightPixels;
		screenDensity = displaymetrics.density;
		
		// Status size
    	int rid = a.getBaseContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
    	if (rid > 0) statusHeight = a.getBaseContext().getResources().getDimensionPixelSize(rid);
		
		// Translucent Decor!
		int translucentDecor = a.getResources().getIdentifier("config_enableTranslucentDecor", "bool", "android");
		if (translucentDecor != 0)
		{
			// Virtual buttons Size
			actionHeight = getVirtualButtonSize(a);
	    	screenHeight += actionHeight;
		}
		else
		{
			screenHeight -= statusHeight;
			statusHeight = 0;
		}
		
		Log.i("Blockdown", "Screen size is " + screenHeight + " x " + screenWidth);
		
		scale = screenWidth / 1080.0f; // Game was designed for 1080p screen
		Log.i("Blockdown", "Game will be scaled " + scale + " of the original design.");
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	private static int getVirtualButtonSize(Activity a)
	{
		Point realSize = new Point();
		a.getWindowManager().getDefaultDisplay().getRealSize(realSize);

		int size = realSize.y - screenHeight;
		
		return size;
	}
	
	public static int getScreenWidth()
	{
		return screenWidth;
	}
	
	public static int getScreenHeight()
	{
		return screenHeight;
	}
	
	public static int getActionBarSize()
	{
		return actionHeight;
	}
	
	public static int getStatusBarSize()
	{
		return statusHeight;
	}
	
	public static float getDensity()
	{
		return screenDensity;
	}
	
	public static void scaleSprite(Sprite e)
	{
		e.setWidth(e.getTextureRegion().getWidth() * scale);
		e.setHeight(e.getTextureRegion().getHeight() * scale);
	}
}
