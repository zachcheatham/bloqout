package me.zachcheatham.blockdown.util.game;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadPlayerScoreResult;

public class Leaderboard
{
	public final static String MAIN = "CgkIr_T2ovYCEAIQAg";
	
	private static int mainHighScore = 0;
	
	public static void submitMainScore(GoogleApiClient api, int score)
	{
		Log.i("Blockdown", "Recieved request to submit a score of " + score);
		
		if (score > mainHighScore)
		{
			Log.i("Blockdown", "Submitting score to Google Plus...");
			Games.Leaderboards.submitScore(api, MAIN, score);
			mainHighScore = score;
		}
	}

	public static void fetchHighScores(GoogleApiClient api)
	{
		Games.Leaderboards.loadCurrentPlayerLeaderboardScore(api, MAIN, LeaderboardVariant.TIME_SPAN_ALL_TIME, LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(new ResultCallback<LoadPlayerScoreResult>()
		{
			@Override
			public void onResult(LoadPlayerScoreResult result)
			{				
				if (result.getScore() != null)
					mainHighScore = (int) result.getScore().getRawScore();
			}
		});
	}
	
	public static int getMainHighScore()
	{
		return mainHighScore;
	}
}
