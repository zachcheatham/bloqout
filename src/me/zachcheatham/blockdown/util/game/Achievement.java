package me.zachcheatham.blockdown.util.game;

public class Achievement
{
	public static final String FIRST_MATCH = "CgkIr_T2ovYCEAIQBA";
	public static final String MULTI_MATCH = "CgkIr_T2ovYCEAIQBQ";
	public static final String TRIPLE_MATCH = "CgkIr_T2ovYCEAIQBg";
	public static final String TWENTY_POINTS = "CgkIr_T2ovYCEAIQBw";
	public static final String MASTER = "CgkIr_T2ovYCEAIQCA";
}
